<?php

class RequisicaoDAO {
    public function all()
    {
        return Requisicao::all();
    }

    public function create(Requisicao $requisicao)
    {
        return $requisicao->save();
    }

    public function update(Requisicao $requisicao)
    {
        return $requisicao->save();
    }

    public function delete(Requisicao $requisicao)
    {
        return $requisicao->delete();
    }

    public function findById($id)
    {
        return Requisicao::findOrFail($id);
    }

    public function allWithStatus($status, $tipo_data = null, $data_inicio = null,
                                  $data_fim = null)
    {
        $query = Requisicao::where('status', '=', $status);
        if (!is_null($tipo_data) && !is_null($data_inicio) && !is_null($data_fim)) {
            $query = $query->where($tipo_data, '>=', $data_inicio);
            $query = $query->where($tipo_data, '<', $data_fim);
        }
        return $query->get();
    }

    public function allWithOutStatus($status, $tipo_data = null, $data_inicio = null,
                                  $data_fim = null)
    {
        $query = Requisicao::where('status', '<>', $status);
        if (!is_null($tipo_data) && !is_null($data_inicio) && !is_null($data_fim)) {
            $query = $query->where($tipo_data, '>=', $data_inicio);
            $query = $query->where($tipo_data, '<', $data_fim);
        }
        return $query->get();
    }
    
    public function naoAgendadasDoUsuario(Usuario $usuario) {
        return $usuario->requisicoesCriadas()
            ->where('status', '=', 'aberto')
            ->orderBy('data_pedido', 'desc')
            ->get();
    }

    public function agendadasDoUsuario(Usuario $usuario) {
        return $usuario->requisicoesCriadas()
            ->where('status', '=', 'atribuido')
            ->orderBy('data_pedido', 'desc')
            ->get();
    }

    public function agendadasDoTecnico(Usuario $usuario) {
        return $usuario->requisicoesAtribuidas()
            ->where('status', '=', 'atribuido')
            ->orderBy('data_atendimento', 'desc')
            ->get();
    }

    public function concluidasDoTecnico(Usuario $usuario, $limit = null) {
        $query = $usuario->requisicoesAtribuidas();
        $query = $query->where('status', '=', 'concluido');
        $query = $query->orderBy('data_atendimento', 'asc');
        if (!is_null($limit)) {
            $query = $query->take($limit);
        }
        return $query->get();
    }

    public function abertasComDepartamentos($departamentos_ids) {
        if (empty($departamentos_ids)) {
            return new \Illuminate\Database\Eloquent\Collection;
        }
        return Requisicao::
            where('status', '=', 'aberto')
            ->whereIn('id_departamento', $departamentos_ids)
            ->orderBy('data_pedido', 'desc')
            ->get();
    }

    public function abertasSemDepartamentos($departamentos_ids) {
        $query = Requisicao::where('status', '=', 'aberto');
        if (!empty($departamentos_ids)) {
            $query = $query->whereNotIn('id_departamento', $departamentos_ids);
        }
        $query = $query->orderBy('data_pedido', 'desc');
        return $query->get();
    }
}