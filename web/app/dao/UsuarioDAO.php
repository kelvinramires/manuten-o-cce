<?php

class UsuarioDAO {
    public function all()
    {
        return Usuario::all();
    }

    public function allPaginated($limit)
    {
        return Usuario::with('departamentos')->paginate($limit);
    }

    public function create(Usuario $usuario)
    {
        return $usuario->save();
    }

    public function update(Usuario $usuario)
    {
        return $usuario->save();
    }

    public function delete(Usuario $usuario)
    {
        return $usuario->delete();
    }

    public function findById($id)
    {
        return Usuario::findOrFail($id);
    }

    public function findByMatriculaOuChapa($matriculaChapa)
    {
        return Usuario::where('matriculachapa', '=', $matriculaChapa)->first();
    }

}