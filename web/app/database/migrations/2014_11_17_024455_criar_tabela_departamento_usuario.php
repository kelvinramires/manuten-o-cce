<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaDepartamentoUsuario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('departamento_usuario', function(Blueprint $table)
		{
			$table->integer('departamento_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->foreign('departamento_id')
                ->references('id')->on('departamentos')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->foreign('usuario_id')
                ->references('id')->on('usuarios')
                ->onUpdate('restrict')
                ->onDelete('cascade');
            $table->primary(['departamento_id', 'usuario_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('departamento_usuario');
	}

}
