<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaRequisicoes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requisicoes', function(Blueprint $table)
		{
            $table->increments('id');
            $table->timestamp('data_atendimento')->nullable();
            $table->timestamp('data_pedido');
            $table->integer('id_departamento')->unsigned();
            $table->integer('id_requisitante')->unsigned();
            $table->integer('id_tecnico')->unsigned()->nullable();
            $table->string('local');
            $table->text('texto');
            $table->foreign('id_departamento')
                ->references('id')->on('departamentos')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->foreign('id_requisitante')
                ->references('id')->on('usuarios')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->foreign('id_tecnico')
                ->references('id')->on('usuarios')
                ->onUpdate('restrict')
                ->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requisicoes');
	}

}
