<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionarStatusEmRequisicoes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('requisicoes', function(Blueprint $table)
		{
			$table->enum('status', ['aberto', 'atribuido', 'concluido', 'cancelado']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('requisicoes', function(Blueprint $table)
		{
			$table->dropColumn('status');
		});
	}

}
