<?php

use Illuminate\Database\Migrations\Migration;

class CriarTabelaUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function ($table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('matriculachapa');
            $table->string('email')->unique();
            $table->timestamp('data_cadastro');
            $table->string('senha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuarios');
    }
}
