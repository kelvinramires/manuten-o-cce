//= require jquery
//= require foundation/foundation
//= require foundation/foundation.topbar
//= require foundation/foundation.reveal
//= require foundation/foundation.equalizer
//= require foundation/foundation.tab
//= require rails
//= require confirm_with_reveal
//= require_self

$(document).foundation();
$(document).on("click", "a[data-disable-link]", function(e) {
    return false;
});
$(document).confirmWithReveal({
    "title": "Tem certeza?",
    "cancel": "Cancelar",
    "ok": "Confimar"
});