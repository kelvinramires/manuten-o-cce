<?php

class Departamento extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'departamentos';

    public $timestamps = false;
    
    public function getKey() {
        return $this->id;
    }
    
    public function usuarios()
    {
        return $this->belongsToMany('Usuario');
    }
}
