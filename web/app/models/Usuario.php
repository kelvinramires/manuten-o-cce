<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class Usuario extends BaseModel implements UserInterface
{
    use UserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('senha');

    public $timestamps = false;

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function getRememberToken()
    {
        return $this->token_de_lembranca;
    }

    public function setRememberToken($value)
    {
        $this->token_de_lembranca = $value;
    }

    public function getRememberTokenName()
    {
        return 'token_de_lembranca';
    }
    
    public function getPapel()
    {
        switch ($this->papel) {
            case "usuario":
            return "usuário";
            case "tecnico":
            return "técnico";
            case "administrador":
            return "administrador";
        }
    }
    
    public function departamentos()
    {
        return $this->belongsToMany('Departamento');
    }

    public function requisicoesCriadas()
    {
        return $this->hasMany('Requisicao', 'id_requisitante');
    }
    
    public function requisicoesAtribuidas()
    {
        return $this->hasMany('Requisicao', 'id_tecnico');
    }
    
    public function checkSenha($senha)
    {
        return Hash::check($senha, $this->senha);
    }
}
