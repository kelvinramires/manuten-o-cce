<?php

class Requisicao extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'requisicoes';
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['data_pedido', 'data_atendimento'];

    public $timestamps = false;
    
    public function tecnico()
    {
        return $this->belongsTo('Usuario', 'id_tecnico');
    }

    public function requisitante()
    {
        return $this->belongsTo('Usuario', 'id_requisitante');
    }
    
    public function departamento()
    {
        return $this->belongsTo('Departamento', 'id_departamento');
    }
    
    public function aceitar($dataAtendimento) {
        $this->status = 'atribuido';
        $this->data_atendimento = $dataAtendimento;
        $this->id_tecnico = Auth::id();
    }
    
    public function abandonar() {
        $this->status = 'aberto';
        $this->data_atendimento = null;
        $this->id_tecnico = null;
    }
    
    public function concluir() {
        $this->status = 'concluido';
    }
}
