<?php

class HistoricoController extends BaseController
{
    /**
     * Instantiate a new HistoricoController instance.
     */
    public function __construct()
    {
        $this->requisicaoDAO = App::make('requisicaoDAO');
        $this->beforeFilter('auth');
        $this->beforeFilter('authorization.administrador||tecnico');
        // $this->beforeFilter('csrf');
    }

    public function filtrarAction()
    {
        $ano_escolhido = Input::get('ano');
        $mes_escolhido = Input::get('mes');
        if ($ano_escolhido && is_numeric($ano_escolhido) && $mes_escolhido && is_numeric($mes_escolhido)) {
            $ano_escolhido = intval($ano_escolhido);
            $mes_escolhido = intval($mes_escolhido);
        } else {
            $mes_escolhido = Date::now()->month;
            $ano_escolhido = Date::now()->year;
        }
        $proximo_mes = $mes_escolhido + 1;
        $data_inicio = "$ano_escolhido-$mes_escolhido-01";
        $data_fim = "$ano_escolhido-$proximo_mes-01";
        $requisicoesAtendidas = $this->requisicaoDAO->allWithStatus(
            'concluido', 'data_atendimento', $data_inicio, $data_fim);
        $requisicoesCriadas = $this->requisicaoDAO->allWithOutStatus(
            'cancelado', 'data_pedido', $data_inicio, $data_fim);
        $requisicoesCanceladas = $this->requisicaoDAO->allWithStatus(
            'cancelado', 'data_pedido', $data_inicio, $data_fim);
        return View::make('historico', [
            'requisicoesAtendidas' => $requisicoesAtendidas,
            'requisicoesCriadas' => $requisicoesCriadas,
            'requisicoesCanceladas' => $requisicoesCanceladas,
            'mes_escolhido' => $mes_escolhido,
            'ano_escolhido' => $ano_escolhido
        ]);
    }
}
