<?php

class LoginController extends BaseController
{
    const cadastroRules = [
        'nome' => 'required',
        'senha' => 'required|min:3|confirmed',
        'email' => 'required|email|confirmed|unique:usuarios',
        'matriculachapa' => 'required|unique:usuarios',
    ];

    const loginRules = [
        'matriculachapa' => 'required',
        'senha' => 'required|min:3',
    ];

    /**
     * Instantiate a new LoginController instance.
     */
    public function __construct()
    {
        $this->usuarioDAO = App::make('usuarioDAO');
        $this->departamentoDAO = App::make('departamentoDAO');
        $this->beforeFilter('auth', ['on' => 'logoutAction']);
        $this->beforeFilter('guest', ['except' => 'logoutAction']);
        // $this->beforeFilter('csrf');
    }

    public function cadastroFormAction()
    {
        $departamentos = $this->departamentoDAO->all()->getDictionary();
        $departamentos = array_map(function($dept) {
            return $dept['nome'];
        }, $departamentos);
        return View::make('login.cadastro', [
            'usuario' => new Usuario(),
            'departamentoOptions' => $departamentos
        ]);
    }

    public function cadastroAction()
    {
        $input = Input::only('nome', 'senha', 'senha_confirmation', 'email',
        'email_confirmation', 'matriculachapa');

        $validator = Validator::make($input, self::cadastroRules);
        if ($validator->fails()) {
            return Redirect::route('cadastro')
                    ->withErrors($validator)
                    ->withInput(Input::only('nome', 'email',
                    'email_confirmation', 'matriculachapa'));
        }
        $usuario = new Usuario();
        $usuario->papel = 'usuario';
        $usuario->nome = $input['nome'];
        $usuario->email = $input['email'];
        $usuario->senha = Hash::make($input['senha']);
        $usuario->matriculachapa = $input['matriculachapa'];
        $this->usuarioDAO->create($usuario);
        return Redirect::to('/');
    }

    public function loginFormAction()
    {
        return View::make('login.login', ['usuario' => new Usuario()]);
    }

    public function loginAction()
    {
        $input = Input::only('matriculachapa', 'senha');

        $validator = Validator::make($input, self::loginRules);
        if ($validator->fails()) {
            return Redirect::route('login')
                    ->withErrors($validator)
                    ->withInput(Input::only('matriculachapa', 'lembre_de_mim'));
        }
        $usuario = 
            $this->usuarioDAO->findByMatriculaOuChapa($input['matriculachapa']);
        if (!is_null($usuario) && $usuario->checkSenha($input['senha'])) {
            $lembrar = (Input::has('lembre_de_mim')) ? true : false;
            Auth::login($usuario, $lembrar);
            return Redirect::to('/');
        } else {
            return Redirect::route('login')
                ->withInput(Input::only('matriculachapa'))
                ->with('global', 'There was a problem logging you in. Please check your credentials and try again.');
        }
    }

    public function logoutAction()
    {
        Auth::logout();
        return Redirect::to('/');
    }
}
