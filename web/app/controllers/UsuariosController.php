<?php
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsuariosController extends BaseController
{

    /**
     * Instantiate a new UsuariosController instance.
     */
    public function __construct()
    {
        $this->usuarioDAO = App::make('usuarioDAO');
        $this->departamentoDAO = App::make('departamentoDAO');
        $this->beforeFilter('auth');
        $this->beforeFilter('authorization.administrador');
        // $this->beforeFilter('csrf');
    }
    
    public function listarAction()
    {
        $usuarios = $this->usuarioDAO->allPaginated(15);
        return View::make('usuarios.listar', [
            'usuarios' => $usuarios
        ]);
    }

    public function editarFormAction($id) {
        $id = intval($id);
        $usuario = $this->usuarioDAO->findById($id);
        $departamentos = $this->departamentoDAO->all();
        return View::make('usuarios.editar', [
            'usuario' => $usuario,
            'departamentos' => $departamentos
        ]);
    }

    public function editarAction($id) {
        $id = intval($id);
        $usuario = $this->usuarioDAO->findById($id);
        
        $input = Input::only('nome', 'senha', 'senha_confirmation', 'email',
        'email_confirmation', 'departamentos');
        $validator = Validator::make($input, [
            // 'papel' => 'required|in:usuario,tecnico,administrador',
            'senha' => 'min:3|confirmed',
            'email' => 'email|confirmed|unique:usuarios,email,'.$id,
            'departamentos' => 'exists:departamentos,id'
        ]);
        if ($validator->fails()) {
            return Redirect::route('usuarios.editar', [$id])
                    ->withErrors($validator)
                    ->withInput(Input::only('nome', 'email', 'email_confirmation', 'papel'));
        }
        if (!empty($input['nome'])) {
            $usuario->nome = $input['nome'];
        }
        if (!empty($input['senha'])) {
            $usuario->senha = Hash::make($input['senha']);
        }
        if (!empty($input['email'])) {
            $usuario->email = $input['email'];
        }
        $usuario->departamentos()->sync($input['departamentos']);
        $this->usuarioDAO->update($usuario);
        return Redirect::route('usuarios.listar');
    }

    public function deletarAction($id) {
        $id = intval($id);
        $usuario = $this->usuarioDAO->findById($id);
        $this->usuarioDAO->delete($usuario);
        $url = URL::previous() ? : URL::route('usuarios.listar');
        return Redirect::to($url);
    }
}
