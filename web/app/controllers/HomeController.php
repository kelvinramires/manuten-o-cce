<?php

class HomeController extends BaseController
{
    /**
     * Instantiate a new HomeController instance.
     */
    public function __construct()
    {
        $this->requisicaoDAO = App::make('requisicaoDAO');
    }
    
    public function indexAction()
    {
        if (Auth::guest()) {
            return View::make('home.visitante');
        }
        $usuario = Auth::user();
        switch ($usuario->papel) {
            case 'usuario':
                $requisicoesNaoAgendadas =
                    $this->requisicaoDAO->naoAgendadasDoUsuario($usuario);
                $requisicoesAgendadas =
                    $this->requisicaoDAO->agendadasDoUsuario($usuario);
                return View::make('home.usuario', [
                    'requisicoesNaoAgendadas' => $requisicoesNaoAgendadas,
                    'requisicoesAgendadas' => $requisicoesAgendadas]);
            case 'tecnico':
                $deptos = $usuario->departamentos->modelKeys();
                $requisicoesNaoAgendadasDpto =
                    $this->requisicaoDAO->abertasComDepartamentos($deptos);
                $requisicoesNaoAgendadasNaoDpto =
                    $this->requisicaoDAO->abertasSemDepartamentos($deptos);
                $requisicoesAgendadas =
                    $this->requisicaoDAO->agendadasDoTecnico($usuario);
                $requisicoesConcluidas =
                    $this->requisicaoDAO->concluidasDoTecnico($usuario, 6);
                return View::make('home.tecnico', [
                    'requisicoesAgendadas' => $requisicoesAgendadas,
                    'requisicoesConcluidas' => $requisicoesConcluidas,
                    'requisicoesNaoAgendadasDpto' => $requisicoesNaoAgendadasDpto,
                    'requisicoesNaoAgendadasNaoDpto' => $requisicoesNaoAgendadasNaoDpto
                ]);
            case 'administrador':
                return View::make('home.administrador');
        }
    }
}
