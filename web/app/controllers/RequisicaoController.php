<?php
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RequisicaoController extends BaseController
{
    const criarRules = [
        'departamento' => 'required|exists:departamentos,id',
        'local' => 'required',
        'texto' => 'required',
    ];

    /**
     * Instantiate a new RequisicaoController instance.
     */
    public function __construct()
    {
        $this->requisicaoDAO = App::make('requisicaoDAO');
        $this->departamentoDAO = App::make('departamentoDAO');
        $this->beforeFilter('auth');
        // $this->beforeFilter('csrf');
    }
    
    public function criarFormAction()
    {
        $departamentos = $this->departamentoDAO->all()->getDictionary();
        $departamentos = array_map(function($dept) {
            return $dept['nome'];
        }, $departamentos);
        $departamentos = array_unshift_assoc($departamentos, '', '');
        return View::make('requisicao.criar', [
            'requisicao' => new Requisicao(),
            'departamentoOptions' => $departamentos
        ]);
    }

    public function criarAction()
    {
        $input = Input::only('departamento', 'local', 'texto');

        $validator = Validator::make($input, self::criarRules);
        if ($validator->fails()) {
            return Redirect::route('requisicao.criar')
                    ->withErrors($validator)
                    ->withInput($input);
        }
        $requisicao = new Requisicao();
        $requisicao->data_pedido = new Carbon();
        $requisicao->id_departamento = $input['departamento'];
        $requisicao->id_requisitante = Auth::id();
        $requisicao->local = $input['local'];
        $requisicao->texto = $input['texto'];
        $this->requisicaoDAO->create($requisicao);

        return Redirect::route('requisicao.mostrar', [$requisicao->id]);
    }
    
    public function mostrarAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        return View::make('requisicao.mostrar', [
            'requisicao' => $requisicao
        ]);
    }

    public function aceitarFormAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        return View::make('requisicao.aceitar', [
            'requisicao' => $requisicao
        ]);
    }

    public function aceitarAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        $input = Input::only('data_atendimento');
        $requisicao->aceitar(Date::parse($input['data_atendimento']));
        $this->requisicaoDAO->update($requisicao);
        return Redirect::route('requisicao.mostrar', [$requisicao->id]);
    }

    public function abandonarAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        $requisicao->abandonar();
        $this->requisicaoDAO->update($requisicao);
        return Redirect::route('requisicao.mostrar', [$requisicao->id]);
    }

    public function atualizarFormAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        return View::make('requisicao.atualizar', [
            'requisicao' => $requisicao
        ]);
    }

    public function atualizarAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        $input = Input::only('data_atendimento');
        $requisicao->data_atendimento = Date::parse($input['data_atendimento']);
        $this->requisicaoDAO->update($requisicao);
        return Redirect::route('requisicao.mostrar', [$requisicao->id]);
    }

    public function concluirAction($id)
    {
        $id = intval($id);
        $requisicao = $this->requisicaoDAO->findById($id);
        $requisicao->concluir();
        $this->requisicaoDAO->update($requisicao);
        return Redirect::route('requisicao.mostrar', [$requisicao->id]);
    }
}
