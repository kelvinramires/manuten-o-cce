<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'root',
    'uses' => 'HomeController@indexAction'
]);
Route::get('/cadastro', [
    'as' => 'cadastro',
    'uses' => 'LoginController@cadastroFormAction'
]);
Route::post('/cadastro', 'LoginController@cadastroAction');
Route::get('/login', [
    'as' => 'login',
    'uses' => 'LoginController@loginFormAction'
]);
Route::post('/login', 'LoginController@loginAction');
Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'LoginController@logoutAction'
]);
Route::get('requisicao/criar', [
    'as' => 'requisicao.criar',
    'uses' => 'RequisicaoController@criarFormAction'
]);
Route::post('requisicao/criar', [
    'uses' => 'RequisicaoController@criarAction'
]);
Route::get('requisicao/{id}', [
    'as' => 'requisicao.mostrar',
    'uses' => 'RequisicaoController@mostrarAction'
])->where('id', '[1-9][0-9]*');
Route::get('requisicao/{id}/aceitar', [
    'as' => 'requisicao.aceitar',
    'uses' => 'RequisicaoController@aceitarFormAction'
])->where('id', '[1-9][0-9]*');
Route::post('requisicao/{id}/aceitar', [
    'uses' => 'RequisicaoController@aceitarAction'
])->where('id', '[1-9][0-9]*');
Route::post('requisicao/{id}/abandonar', [
    'as' => 'requisicao.abandonar',
    'uses' => 'RequisicaoController@abandonarAction'
])->where('id', '[1-9][0-9]*');
Route::get('requisicao/{id}/atualizar', [
    'as' => 'requisicao.atualizar',
    'uses' => 'RequisicaoController@atualizarFormAction'
])->where('id', '[1-9][0-9]*');
Route::post('requisicao/{id}/atualizar', [
    'uses' => 'RequisicaoController@atualizarAction'
])->where('id', '[1-9][0-9]*');
Route::post('requisicao/{id}/concluir', [
    'as' => 'requisicao.concluir',
    'uses' => 'RequisicaoController@concluirAction'
])->where('id', '[1-9][0-9]*');
Route::post('requisicao/{id}/cancelar', [
    'as' => 'requisicao.cancelar',
    'uses' => 'RequisicaoController@cancelarAction'
])->where('id', '[1-9][0-9]*');
Route::get('admin/usuarios', [
    'as' => 'usuarios.listar',
    'uses' => 'UsuariosController@listarAction'
]);
Route::get('admin/usuarios/{id}/editar', [
    'as' => 'usuarios.editar',
    'uses' => 'UsuariosController@editarFormAction'
])->where('id', '[1-9][0-9]*');
Route::post('admin/usuarios/{id}/editar', [
    'uses' => 'UsuariosController@editarAction'
])->where('id', '[1-9][0-9]*');
Route::delete('admin/usuarios/{id}', [
    'as' => 'usuarios.deletar',
    'uses' => 'UsuariosController@deletarAction'
])->where('id', '[1-9][0-9]*');

Route::get('historico', [
    'as' => 'historico',
    'uses' => 'HistoricoController@filtrarAction'
]);