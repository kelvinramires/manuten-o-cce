<?php

App::singleton('usuarioDAO', function()
{
    return new UsuarioDAO();
});

App::singleton('requisicaoDAO', function()
{
    return new RequisicaoDAO();
});

App::singleton('departamentoDAO', function()
{
    return new DepartamentoDAO();
});