@extends('layout')
@section('title', 'Manutenção CCE | Cadastro')
@section('content')
		<h1 id="my-title">Novo Cadastro</h1>
	{{ Form::model($usuario, array('route' => 'cadastro', 'method' => 'post')) }}
		<div class="row">
			<div class="large-6 medium-6 columns">
				{{ Form::label('nome', 'Nome') }}
				{{ Form::text('nome', $usuario->nome) }}
			</div>
			<div class="large-6 medium-6 columns">
				{{ Form::label('matriculachapa', 'Matrícula ou Chapa') }}
				{{ Form::text('matriculachapa') }}
			</div>
		</div>
		<div class="row">
			<div class="large-6 medium-6 columns">
				{{ Form::label('email', 'E-mail') }}
				{{ Form::text('email', $usuario->email) }}
			</div>
			<div class="large-6 medium-6 columns">
				{{ Form::label('email_confirmation', 'Confirmação de e-mail') }}
				{{ Form::text('email_confirmation', $usuario->email) }}
			</div>
		</div>
		<div class="row">
			<div class="large-6 medium-6 columns">
				{{ Form::label('senha', 'Senha') }}
				{{ Form::password('senha') }}
			</div>
			<div class="large-6 medium-6 columns">
				{{ Form::label('senha_confirmation', 'Confirmação de senha') }}
				{{ Form::password('senha_confirmation') }}
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
                {{ Form::label('departamento', "Escolha seu departamento") }}
                {{ Form::select('departamento', $departamentoOptions) }}
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<input type="submit" class="button" value="Cadastrar">
			</div>
		</div>
	{{ Form::close() }}
@stop
