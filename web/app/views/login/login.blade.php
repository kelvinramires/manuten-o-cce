@extends('layout')
@section('title', 'Manutenção CCE | Login')
@section('content')
	<h1 id="my-title">Login</h1>
	{{ Form::model($usuario, array('route' => 'login', 'method' => 'post')) }}
		<div class="row">
			<div class="large-6 large-centered medium-6 columns">
                <div>
    				{{ Form::label('matriculachapa', 'Matrícula ou Chapa') }}
    				{{ Form::text('matriculachapa') }}
                </div>
                <div>
    				{{ Form::label('senha', 'Senha') }}
    				{{ Form::password('senha') }}
                </div>
                <div>
    				{{ Form::checkbox('lembre_de_mim', null, null, ['id' => 'lembre_de_mim']) }}
                    {{ Form::label('lembre_de_mim', 'Continuar conectado') }}
                </div>
				<input type="submit" class="button expand" value="Login">
			</div>
		</div>
	{{ Form::close() }}
@stop
