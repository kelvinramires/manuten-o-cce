<div class="large-6 columns">
    <div class="requisicao-block" data-equalizer-watch>
        <a href="{{ URL::route('requisicao.mostrar', [$requisicao->id])}}">
        <div>
            <i class="fa fa-clock-o"></i>
            Criada {{ $requisicao->data_pedido->ago() }}
        </div>
        @if (!is_null($requisicao->data_atendimento))
            <div>
                <i class="fa fa-clock-o"></i>
                @if ($requisicao->status == 'atribuido')
                    Agendada para
                @else
                    Realizada
                @endif
                @if ($requisicao->data_atendimento->isYesterday())
                    ontem às {{{ $requisicao->data_atendimento->toTimeString() }}}
                @elseif($requisicao->data_atendimento->isToday())
                    hoje às {{{ $requisicao->data_atendimento->toTimeString() }}}
                @elseif($requisicao->data_atendimento->isTomorrow())
                    amanhã às {{{ $requisicao->data_atendimento->toTimeString() }}}
                @else
                    {{{ $requisicao->data_atendimento->toDateTimeString() }}}
                @endif
            </div>
        @endif
        <div>
            <i class="fa fa-map-marker"></i>
            {{{ $requisicao->departamento->nome }}},
            em "{{{ $requisicao->local }}}"
        </div>
        <div>
            {{ str_limit($requisicao->texto) }}
        </div>
        </a>
    </div>
</div>
