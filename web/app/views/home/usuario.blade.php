@extends('layout')
@section('content')
<h1 id="my-title">Bem Vindo {{{ Auth::user()->nome }}}</h1>
<div class="row">
    <div class="columns large-6 medium-6">
        <h4>Requisições aguardando agendamento</h3>
        @foreach ($requisicoesNaoAgendadas->chunk(2) as $requisicoes)
            <div class="row" data-equalizer>
            @foreach($requisicoes as $requisicao)
                @include('home.requisicao-block', ['requisicao' => $requisicao])
            @endforeach
            </div>
        @endforeach
    </div>
    <div class="columns large-6 medium-6">
        <h4>Requisições agendadas</h3>
        @if ($requisicoesAgendadas->isEmpty())
            @if ($requisicoesNaoAgendadas->isEmpty())
            <p>
                Você não tem nenhuma requisição pendente nem agendada, deseja <a href="{{ URL::route('requisicao.criar') }}">criar uma requisição</a>?
            </p>
            @else
            <p>Nenhuma de suas requisições foi agendada</p>
            <p style="font-size: 0.85rem">Por favor aguarde, quando alguma de suas requisições for agendada você receberá um e-mail avisando quando será o atendimento.</p>
            @endif
        @else
            @foreach ($requisicoesAgendadas->chunk(2) as $requisicoes)
                <div class="row" data-equalizer>
                @foreach($requisicoes as $requisicao)
                    @include('home.requisicao-block', ['requisicao' => $requisicao])
                @endforeach
                </div>
            @endforeach
        @endif
    </div>
</div>

@endsection