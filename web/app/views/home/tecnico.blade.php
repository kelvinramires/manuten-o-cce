@extends('layout')
@section('content')
<h1 id="my-title">Bem Vindo {{{ Auth::user()->nome }}}</h1>
<div class="row">
    <div class="columns medium-6 large-6">
        <h4>Requisições agendadas a você</h3>
        @if ($requisicoesAgendadas->isEmpty())
            Não existem requisições agendadas a você
        @else
            @foreach ($requisicoesAgendadas->chunk(2) as $requisicoes)
                <div class="row" data-equalizer>
                @foreach ($requisicoes as $requisicao)
                    @include('home.requisicao-block', ['requisicao' => $requisicao])
                @endforeach
                </div>
            @endforeach
        @endif
    </div>
    <div class="columns medium-6 large-6">
        <h4>Requisições não agendadas de seus departamentos</h3>
        @if ($requisicoesNaoAgendadasDpto->isEmpty())
            Não existem requisições não agendadas de seus departamentos
        @else
        @foreach ($requisicoesNaoAgendadasDpto->chunk(2) as $requisicoes)
            <div class="row" data-equalizer>
            @foreach ($requisicoes as $requisicao)
                @include('home.requisicao-block', ['requisicao' => $requisicao])
            @endforeach
            </div>
        @endforeach
        @endif
    </div>
</div>
<div class="row">
    <div class="columns medium-push-6 medium-6 large-push-6 large-6">
        <h4>Requisições não agendadas de outros departamentos</h3>
        @if ($requisicoesNaoAgendadasNaoDpto->isEmpty())
            Não existem requisições não agendadas de outros departamentos
        @else
            @foreach ($requisicoesNaoAgendadasNaoDpto->chunk(2) as $requisicoes)
                <div class="row" data-equalizer>
                @foreach ($requisicoes as $requisicao)
                    @include('home.requisicao-block', ['requisicao' => $requisicao])
                @endforeach
                </div>
            @endforeach
        @endif
    </div>
    <div class="columns medium-pull-6 medium-6 large-pull-6 large-6">
        <h4>Requisições concluídas por você</h3>
        @if ($requisicoesConcluidas->isEmpty())
            Não existem requisições concluídas por você
        @else
            @foreach ($requisicoesConcluidas->chunk(2) as $requisicoes)
                <div class="row" data-equalizer>
                @foreach ($requisicoes as $requisicao)
                    @include('home.requisicao-block', ['requisicao' => $requisicao])
                @endforeach
                </div>
            @endforeach
        @endif
    </div>
</div>
@endsection