@extends('layout')
@section('title', 'Manutenção CCE | Aceitar requisição')
@section('content')
	<h1 id="my-title">Aceitar requisição</h1>
	{{ Form::open(array('route' => ['requisicao.aceitar', $requisicao->id], 'method' => 'post')) }}
    	<div class="row">
    		<div class="large-6 columns">
                {{ Form::label('data_atendimento', "Data programada para o atendimento desta requisição") }}
                {{ Form::datetimelocal('data_atendimento') }}
    		</div>
    	</div>
		<div class="row">
			<div class="large-6 columns">
				<input type="submit" class="button" value="Aceitar Requisição">
			</div>
		</div>
	{{ Form::close() }}
@stop
