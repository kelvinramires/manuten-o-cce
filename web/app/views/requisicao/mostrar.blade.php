@extends('layout')
@section('title', "Manutenção CCE | Requisição #$requisicao->id")
@section('content')
<h1 id="my-title">Requisição #{{ $requisicao->id }}</h1>
<div class="row">
    <div class="columns large-centered large-8 clearfix">
        @if (Auth::id() === $requisicao->id_tecnico && $requisicao->status == 'atribuido')
            <ul class="stack button-group right">
                <li><a href="{{ URL::route('requisicao.abandonar', [$requisicao->id]) }}" data-confirm='{"body":"Você tem certeza que deseja abandonar esta requisição?", "ok": "Abandonar"}' data-method="post" class="small alert button">
                    Abandonar
                </a></li>
                <li><a href="{{ URL::route('requisicao.atualizar', [$requisicao->id]) }}" class="small button">
                    Trocar data
                </a></li>
                <li><a href="{{ URL::route('requisicao.concluir', [$requisicao->id]) }}" data-confirm='{"body":"Você tem certeza que deseja concluir esta requisição?", "ok": "Concluir", "ok_class": "button"}' data-method="post" class="small button">
                    Concluir
                </a></li>
            </ul>
        @elseif (Auth::user()->papel == 'tecnico' && $requisicao->status == 'aberto')
            <a href="{{ URL::route('requisicao.aceitar', [$requisicao->id]) }}" class="button small right">
                Aceitar
            </a>
        @elseif (Auth::id() === $requisicao->id_requisitante)
            <a href="{{ URL::route('requisicao.cancelar', [$requisicao->id]) }}" class="button small right">
                Cancelar
            </a>
        @endif
        <div>
            <i class="fa fa-tag"></i>
            Status: {{{ $requisicao->status }}}
        </div>
        <div>
            <i class="fa fa-clock-o"></i>
            Criada {{ $requisicao->data_pedido->ago() }}
        </div>
        @if (!is_null($requisicao->data_atendimento))
            <div>
                <i class="fa fa-clock-o"></i>
                @if ($requisicao->status == 'atribuido')
                    Agendada para
                @else
                    Realizada
                @endif
                @if ($requisicao->data_atendimento->isYesterday())
                    ontem às {{{ $requisicao->data_atendimento->toTimeString() }}}
                @elseif($requisicao->data_atendimento->isToday())
                    hoje às {{{ $requisicao->data_atendimento->toTimeString() }}}
                @elseif($requisicao->data_atendimento->isTomorrow())
                    amanhã às {{{ $requisicao->data_atendimento->toTimeString() }}}
                @else
                    {{{ $requisicao->data_atendimento->toDateTimeString() }}}
                @endif
            </div>
        @endif
        <div>
            <i class="fa fa-user"></i>
            Criada por {{{ $requisicao->requisitante->nome }}}
        </div>
        @if (!is_null($requisicao->tecnico))
        <div>
            <i class="fa fa-user"></i>
            Atribuída ao técnico {{{ $requisicao->tecnico->nome }}}
        </div>
        @endif
        <div>
            <i class="fa fa-map-marker"></i>
            {{{ $requisicao->departamento->nome }}},
            em "{{{ $requisicao->local }}}"
        </div>
        <div>
            <h5>Descrição</h5>
            <blockquote>
            {{{ $requisicao->texto }}}
            </blockquote>
        </div>
    </div>
</div>
@stop
