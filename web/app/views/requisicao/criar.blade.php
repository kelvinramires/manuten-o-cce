@extends('layout')
@section('title', 'Manutenção CCE | Criar requisição')
@section('content')
	<h1 id="my-title">Criar requisição</h1>
	{{ Form::model($requisicao, array('route' => 'requisicao.criar', 'method' => 'post')) }}
    	<div class="row">
			<div class="large-6 columns">
                {{ Form::label('departamento', "Escolha seu departamento") }}
                {{ Form::select('departamento', $departamentoOptions) }}
			</div>
    		<div class="large-6 columns">
                {{ Form::label('local', "Local") }}
                {{ Form::text('local') }}
    		</div>
    	</div>
		<div class="row">
			<div class="large-12 columns">
                {{ Form::label('texto', "Descreva") }}
                {{ Form::textarea('texto') }}
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<input type="submit" class="button" value="Enviar Requisição">
			</div>
		</div>
	{{ Form::close() }}
@stop
