@extends('layout')
@section('title', 'Manutenção CCE | Editar Usuário')
@section('content')
<h1 id="my-title">Editar Usuário</h1>
{{ Form::model($usuario, ['route' => ['usuarios.editar', $usuario->id], 'method' => 'post']) }}
    <div class="row">
        <div class="large-6 medium-6 columns">
            {{ Form::label('nome', 'Nome') }}
            {{ Form::text('nome', $usuario->nome) }}
        </div>
        <div class="large-6 medium-6 columns">
            {{ Form::label('matriculachapa', 'Matrícula ou Chapa') }}
            {{ Form::text('matriculachapa', $usuario->matriculachapa, ["disabled" => "disabled"]) }}
        </div>
    </div>
    <div class="row">
        <div class="large-6 medium-6 columns">
            {{ Form::label('email', 'E-mail') }}
            {{ Form::text('email', $usuario->email) }}
        </div>
        <div class="large-6 medium-6 columns">
            {{ Form::label('email_confirmation', 'Confirmação de e-mail') }}
            {{ Form::text('email_confirmation', $usuario->email) }}
        </div>
    </div>
    <div class="row">
        <div class="large-6 medium-6 columns">
            <label>Papel</label>
            @foreach (['usuario' => 'Usuário', 'tecnico' => 'Técnico', 'administrador' => 'Administrador'] as $papel => $papelNome)
                {{ Form::radio('papel', $papel) }}
                {{ Form::label('papel', $papelNome) }}
            @endforeach
        </div>
        <div class="large-6 medium-6 columns">
            <label>Departamento associado</label>
            @foreach ($departamentos as $departamento)
                {{ Form::checkbox('departamentos[]', $departamento->id, null, ['id' => 'departamento' . $departamento->id]) }}
                {{ Form::label('departamento' . $departamento->id, $departamento->nome) }}
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="large-6 medium-6 columns">
            {{ Form::label('senha', 'Senha') }}
            {{ Form::password('senha') }}
        </div>
        <div class="large-6 medium-6 columns">
            {{ Form::label('senha_confirmation', 'Confirmação de senha') }}
            {{ Form::password('senha_confirmation') }}
        </div>
    </div>
    <div class="row">
        <div class="large-6 columns">
            <input type="submit" class="button" value="Salvar">
        </div>
    </div>
{{ Form::close() }}
@endsection