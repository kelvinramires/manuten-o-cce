@extends('layout')

@section('content')
<h1 id="my-title">Usuários</h1>
<div class="row">
    <div class="column large-12">
    <table>
        <caption>Usuários</caption>
      <thead>
        <tr>
          <th>ID</th>
          <th>Matrícula ou chapa</th>
          <th>Nome</th>
          <th>E-mail</th>
          <th>Papel</th>
          <th>Departamentos</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
         @foreach ($usuarios as $usuario)
            <tr>
                <td>{{$usuario->id}}</td>
                <td>{{{$usuario->matriculachapa}}}</td>
                <td>{{{$usuario->nome}}}</td>
                <td>{{{$usuario->email}}}</td>
                <td>{{{$usuario->getPapel()}}}</td>
                <td><ul class="inline-list">
                    @foreach ($usuario->departamentos as $departamento)
                        <li>{{{ $departamento->nome }}}</li>
                    @endforeach
                </ul></td>
                <td style="padding: 0;"><ul class="stack round button-group">
                    <li><a href="{{ URL::route('usuarios.editar', [$usuario->id]) }}" class="tiny button">
                        <i class="fa fa-edit"></i>
                        Editar
                    </a></li>
                    <li><a href="{{ URL::route('usuarios.deletar', [$usuario->id]) }}" data-confirm='{"body":"Você tem certeza que deseja excluir este usuário \"{{{$usuario->nome}}}\"?", "ok": "Excluir"}' data-method="delete" class="tiny alert button">
                        <i class="fa fa-trash"></i>
                        Excluir
                    </a></li>
                </ul></td>
            </tr>
          @endforeach
      </tbody>
    </table>
    {{ $usuarios->links() }}
</div>
</div>
@stop
