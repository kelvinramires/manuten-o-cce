<!doctype html>
<html class="no-js" lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-param" content="_token">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{ URL::to('/favicon.ico') }}" />
        <title>@yield('title', 'Manutenção CCE')</title>
        {{ stylesheet_link_tag() }}
        <script src="/assets/modernizr.js"></script>
    </head>
    <body>
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="/">Manutenção CCE</a></h1>
                </li>
                {{-- Remove  the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone --}}
                <li class="toggle-topbar menu-icon">
                    <a href="#"><span>Menu</span></a>
                </li>
            </ul> {{-- </ul class="title-area"> --}}
            <section class="top-bar-section">
                {{-- Right Nav Section --}}
                <ul class="right">
                    <li class="divider"></li>
                    @if (Auth::check())
                        <li class="has-dropdown">
                            <a href="#" data-disable-link>
                                Bem Vindo {{{ Auth::user()->nome }}}
                            </a>
                            <ul class="dropdown">
                                <li><a href="{{ route('logout') }}">
                                    <i class="fa fa-sign-out"></i>
                                    Logout
                                </a></li>
                            </ul>
                        </li>
                    @else
                        <li><a href="/login">Faça Login</a></li>
                        <li class="divider"></li>
                        <li><a href="/cadastro">Cadastre-se</a></li>
                    @endif
                </ul>{{-- </ul class="right"> --}}
            </section>{{-- </section class="top-bar-section"> --}}
        </nav>{{-- </nav class="top-bar"> --}}
        @yield('content')
        {{ javascript_include_tag() }}
    </body>
</html>
