@extends('layout')
@section('content')
<h1 id="my-title">Histórico</h1>
<div class="row">
    @foreach (["Jan", "Fev", "Mar", "Abr", "Maio", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"] as $idx => $mes)
    <div class="columns medium-2 large-1">
        @if (($idx + 1) === $mes_escolhido)
            <a href="{{ URL::route('historico', ['ano' => $ano_escolhido, 'mes' => $idx + 1]) }}" class="button expand">
                <div>{{$ano_escolhido}}</div>
                <div>{{$mes}}</div>
            </a>
        @else
            <a href="{{ URL::route('historico', ['ano' => $ano_escolhido, 'mes' => $idx + 1]) }}" class="button expand secondary">
                <div>{{$ano_escolhido}}</div>
                <div>{{$mes}}</div>
            </a>
        @endif
    </div>
    @endforeach
</div>
<div class="row">
    <div class="columns large-12">
        <dl class="tabs" data-tab>
          <dd class="active"><a href="#requisicoes-atendidas">
              Requisições atendidas
              <span class="secondary radius label">
                  {{ $requisicoesAtendidas->count() }}
              </span>
          </a></dd>
          <dd><a href="#requisicoes-criadas">
              Requisições criadas
              <span class="secondary radius label">
                  {{ $requisicoesCriadas->count() }}
              </span>
          </a></dd>
          <dd><a href="#requisicoes-canceladas">
              Requisições canceladas
              <span class="secondary radius label">
                  {{ $requisicoesCanceladas->count() }}
              </span>
          </a></dd>
        </dl>
        <div class="tabs-content">
          <div class="content active" id="requisicoes-atendidas">
            <p>
                @if ($requisicoesAtendidas->isEmpty())
                    Não existem requisições atendidas neste período
                @else
                    @foreach ($requisicoesAtendidas->chunk(2) as $chunk)
                        <div class="row" data-equalizer>
                        @foreach ($chunk as $requisicao)
                            @include('home.requisicao-block', ['requisicao' => $requisicao])
                        @endforeach
                        </div>
                    @endforeach
                @endif
            </p>
          </div>
          <div class="content" id="requisicoes-criadas">
            <p>
                @if ($requisicoesCriadas->isEmpty())
                    Não existem requisições criadas neste período
                @else
                    @foreach ($requisicoesCriadas->chunk(2) as $chunk)
                        <div class="row" data-equalizer>
                        @foreach ($chunk as $requisicao)
                            @include('home.requisicao-block', ['requisicao' => $requisicao])
                        @endforeach
                        </div>
                    @endforeach
                @endif
            </p>
          </div>
          <div class="content" id="requisicoes-canceladas">
            <p>
                @if ($requisicoesCanceladas->isEmpty())
                    Não existem requisições canceladas neste período
                @else
                    @foreach ($requisicoesCanceladas->chunk(2) as $chunk)
                        <div class="row" data-equalizer>
                        @foreach ($chunk as $requisicao)
                            @include('home.requisicao-block', ['requisicao' => $requisicao])
                        @endforeach
                        </div>
                    @endforeach
                @endif
            </p>
          </div>
        </div>
    </div>
</div>
@endsection